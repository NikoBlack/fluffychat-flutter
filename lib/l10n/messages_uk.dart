// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a uk locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'uk';

  static m0(username) => "${username} прийняв(ла) запрошення";

  static m1(username) => "${username} активував(ла) наскрізне шифрування";

  static m2(username) => "Прийняти цей запит на підтвердження від ${username}?";

  static m3(username, targetName) => "${username} забанив(ла) ${targetName}";

  static m4(homeserver) =>
      "За замовчуванням ви будете підключені до ${homeserver}";

  static m5(username) => "${username} змінив(ла) аватар чату";

  static m6(username, description) =>
      "${username} змінив(ла) опис чату на: \'${description}\'";

  static m7(username, chatname) =>
      "${username} змінив(ла) ім\'я чату на: \'${chatname}\'";

  static m8(username) => "";

  static m9(username, displayname) => "";

  static m10(username) => "";

  static m11(username, rules) => "";

  static m12(username) => "";

  static m13(username, rules) => "";

  static m14(username) => "";

  static m15(username, joinRules) => "";

  static m16(username) => "";

  static m17(username) => "";

  static m18(username) => "";

  static m19(error) => "";

  static m20(count) => "";

  static m21(username) => "";

  static m22(date, timeOfDay) => "";

  static m23(year, month, day) => "${day}.${month}.${year}";

  static m24(month, day) => "";

  static m25(displayname) => "";

  static m26(username, targetName) => "";

  static m27(groupName) => "";

  static m28(username, link) => "";

  static m29(username, targetName) => "";

  static m30(username) => "";

  static m31(username, targetName) => "";

  static m32(username, targetName) => "";

  static m33(localizedTimeShort) => "";

  static m34(count) => "";

  static m35(homeserver) => "";

  static m36(number) => "";

  static m37(fileName) => "";

  static m38(username) => "";

  static m39(username) => "";

  static m40(username) => "";

  static m41(username) => "";

  static m42(username, count) => "";

  static m43(username, username2) => "";

  static m44(username) => "";

  static m45(username) => "";

  static m46(username) => "";

  static m47(username) => "";

  static m48(username) => "";

  static m49(username) => "";

  static m50(hours12, hours24, minutes, suffix) => "${hours24}:${minutes}";

  static m51(username, targetName) => "";

  static m52(type) => "";

  static m53(unreadCount) => "";

  static m54(unreadEvents) => "";

  static m55(unreadEvents, unreadChats) => "";

  static m56(username, count) => "";

  static m57(username, username2) => "";

  static m58(username) => "";

  static m59(username) => "";

  static m60(username, type) => "";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function>{
        "(Optional) Group name": MessageLookupByLibrary.simpleMessage(""),
        "About": MessageLookupByLibrary.simpleMessage("Про програму"),
        "Accept": MessageLookupByLibrary.simpleMessage("Прийняти"),
        "Account": MessageLookupByLibrary.simpleMessage("Обліковий запис"),
        "Account informations": MessageLookupByLibrary.simpleMessage(
            "Інформація про обліковий запис"),
        "Add a group description":
            MessageLookupByLibrary.simpleMessage("Додати опис групи"),
        "Admin": MessageLookupByLibrary.simpleMessage("Адміністратор"),
        "Already have an account?":
            MessageLookupByLibrary.simpleMessage("Вже маєте обліковий запис?"),
        "Anyone can join":
            MessageLookupByLibrary.simpleMessage("Будь-хто може приєднатись"),
        "Archive": MessageLookupByLibrary.simpleMessage("Архів"),
        "Archived Room":
            MessageLookupByLibrary.simpleMessage("Заархівована кімната"),
        "Are guest users allowed to join": MessageLookupByLibrary.simpleMessage(
            "Чи дозволено гостям приєднуватись"),
        "Are you sure?": MessageLookupByLibrary.simpleMessage("Ви впевнені?"),
        "Authentication":
            MessageLookupByLibrary.simpleMessage("Аутентифікація"),
        "Avatar has been changed":
            MessageLookupByLibrary.simpleMessage("Аватар був змінений"),
        "Ban from chat":
            MessageLookupByLibrary.simpleMessage("Забанити в чаті"),
        "Banned": MessageLookupByLibrary.simpleMessage("Забанений(на)"),
        "Block Device":
            MessageLookupByLibrary.simpleMessage("Заблокувати пристрій"),
        "Cancel": MessageLookupByLibrary.simpleMessage("Скасувати"),
        "Change the homeserver": MessageLookupByLibrary.simpleMessage(""),
        "Change the name of the group":
            MessageLookupByLibrary.simpleMessage(""),
        "Change the server": MessageLookupByLibrary.simpleMessage(""),
        "Change wallpaper": MessageLookupByLibrary.simpleMessage(""),
        "Change your style": MessageLookupByLibrary.simpleMessage(""),
        "Changelog": MessageLookupByLibrary.simpleMessage(""),
        "Chat": MessageLookupByLibrary.simpleMessage(""),
        "Chat details": MessageLookupByLibrary.simpleMessage(""),
        "Choose a strong password": MessageLookupByLibrary.simpleMessage(""),
        "Choose a username": MessageLookupByLibrary.simpleMessage(""),
        "Close": MessageLookupByLibrary.simpleMessage(""),
        "Confirm": MessageLookupByLibrary.simpleMessage(""),
        "Connect": MessageLookupByLibrary.simpleMessage(""),
        "Connection attempt failed": MessageLookupByLibrary.simpleMessage(""),
        "Contact has been invited to the group":
            MessageLookupByLibrary.simpleMessage(""),
        "Content viewer": MessageLookupByLibrary.simpleMessage(""),
        "Copied to clipboard": MessageLookupByLibrary.simpleMessage(""),
        "Copy": MessageLookupByLibrary.simpleMessage(""),
        "Could not set avatar": MessageLookupByLibrary.simpleMessage(""),
        "Could not set displayname": MessageLookupByLibrary.simpleMessage(""),
        "Create": MessageLookupByLibrary.simpleMessage(""),
        "Create account now": MessageLookupByLibrary.simpleMessage(""),
        "Create new group": MessageLookupByLibrary.simpleMessage(""),
        "Currently active": MessageLookupByLibrary.simpleMessage(""),
        "Dark": MessageLookupByLibrary.simpleMessage(""),
        "Delete": MessageLookupByLibrary.simpleMessage(""),
        "Delete message": MessageLookupByLibrary.simpleMessage(""),
        "Deny": MessageLookupByLibrary.simpleMessage(""),
        "Device": MessageLookupByLibrary.simpleMessage(""),
        "Devices": MessageLookupByLibrary.simpleMessage(""),
        "Discard picture": MessageLookupByLibrary.simpleMessage(""),
        "Displayname has been changed":
            MessageLookupByLibrary.simpleMessage(""),
        "Donate": MessageLookupByLibrary.simpleMessage(""),
        "Download file": MessageLookupByLibrary.simpleMessage(""),
        "Edit Jitsi instance": MessageLookupByLibrary.simpleMessage(""),
        "Edit displayname": MessageLookupByLibrary.simpleMessage(""),
        "Emote Settings": MessageLookupByLibrary.simpleMessage(""),
        "Emote shortcode": MessageLookupByLibrary.simpleMessage(""),
        "Empty chat": MessageLookupByLibrary.simpleMessage(""),
        "Encryption": MessageLookupByLibrary.simpleMessage(""),
        "Encryption algorithm": MessageLookupByLibrary.simpleMessage(""),
        "Encryption is not enabled": MessageLookupByLibrary.simpleMessage(""),
        "End to end encryption is currently in Beta! Use at your own risk!":
            MessageLookupByLibrary.simpleMessage(""),
        "End-to-end encryption settings":
            MessageLookupByLibrary.simpleMessage(""),
        "Enter a group name": MessageLookupByLibrary.simpleMessage(""),
        "Enter a username": MessageLookupByLibrary.simpleMessage(""),
        "Enter your homeserver": MessageLookupByLibrary.simpleMessage(""),
        "File name": MessageLookupByLibrary.simpleMessage(""),
        "File size": MessageLookupByLibrary.simpleMessage(""),
        "FluffyChat": MessageLookupByLibrary.simpleMessage(""),
        "Forward": MessageLookupByLibrary.simpleMessage(""),
        "Friday": MessageLookupByLibrary.simpleMessage(""),
        "From joining": MessageLookupByLibrary.simpleMessage(""),
        "From the invitation": MessageLookupByLibrary.simpleMessage(""),
        "Group": MessageLookupByLibrary.simpleMessage(""),
        "Group description": MessageLookupByLibrary.simpleMessage(""),
        "Group description has been changed":
            MessageLookupByLibrary.simpleMessage(""),
        "Group is public": MessageLookupByLibrary.simpleMessage(""),
        "Guests are forbidden": MessageLookupByLibrary.simpleMessage(""),
        "Guests can join": MessageLookupByLibrary.simpleMessage(""),
        "Help": MessageLookupByLibrary.simpleMessage(""),
        "Homeserver is not compatible":
            MessageLookupByLibrary.simpleMessage(""),
        "How are you today?": MessageLookupByLibrary.simpleMessage(""),
        "ID": MessageLookupByLibrary.simpleMessage(""),
        "Identity": MessageLookupByLibrary.simpleMessage(""),
        "Invite contact": MessageLookupByLibrary.simpleMessage(""),
        "Invited": MessageLookupByLibrary.simpleMessage(""),
        "Invited users only": MessageLookupByLibrary.simpleMessage(""),
        "It seems that you have no google services on your phone. That\'s a good decision for your privacy! To receive push notifications in FluffyChat we recommend using microG: https://microg.org/":
            MessageLookupByLibrary.simpleMessage(""),
        "Kick from chat": MessageLookupByLibrary.simpleMessage(""),
        "Last seen IP": MessageLookupByLibrary.simpleMessage(""),
        "Leave": MessageLookupByLibrary.simpleMessage(""),
        "Left the chat": MessageLookupByLibrary.simpleMessage(""),
        "License": MessageLookupByLibrary.simpleMessage(""),
        "Light": MessageLookupByLibrary.simpleMessage(""),
        "Load more...": MessageLookupByLibrary.simpleMessage(""),
        "Loading... Please wait": MessageLookupByLibrary.simpleMessage(""),
        "Login": MessageLookupByLibrary.simpleMessage(""),
        "Logout": MessageLookupByLibrary.simpleMessage(""),
        "Make a moderator": MessageLookupByLibrary.simpleMessage(""),
        "Make an admin": MessageLookupByLibrary.simpleMessage(""),
        "Make sure the identifier is valid":
            MessageLookupByLibrary.simpleMessage(""),
        "Message will be removed for all participants":
            MessageLookupByLibrary.simpleMessage(""),
        "Moderator": MessageLookupByLibrary.simpleMessage(""),
        "Monday": MessageLookupByLibrary.simpleMessage(""),
        "Mute chat": MessageLookupByLibrary.simpleMessage(""),
        "New message in FluffyChat": MessageLookupByLibrary.simpleMessage(""),
        "New private chat": MessageLookupByLibrary.simpleMessage(""),
        "No emotes found. 😕": MessageLookupByLibrary.simpleMessage(""),
        "No permission": MessageLookupByLibrary.simpleMessage(""),
        "No rooms found...": MessageLookupByLibrary.simpleMessage(""),
        "None": MessageLookupByLibrary.simpleMessage(""),
        "Not supported in web": MessageLookupByLibrary.simpleMessage(""),
        "Oops something went wrong...":
            MessageLookupByLibrary.simpleMessage(""),
        "Open app to read messages": MessageLookupByLibrary.simpleMessage(""),
        "Open camera": MessageLookupByLibrary.simpleMessage(""),
        "Participating user devices": MessageLookupByLibrary.simpleMessage(""),
        "Password": MessageLookupByLibrary.simpleMessage(""),
        "Pick image": MessageLookupByLibrary.simpleMessage(""),
        "Please be aware that you need Pantalaimon to use end-to-end encryption for now.":
            MessageLookupByLibrary.simpleMessage(""),
        "Please choose a username": MessageLookupByLibrary.simpleMessage(""),
        "Please enter a matrix identifier":
            MessageLookupByLibrary.simpleMessage(""),
        "Please enter your password": MessageLookupByLibrary.simpleMessage(""),
        "Please enter your username": MessageLookupByLibrary.simpleMessage(""),
        "Public Rooms": MessageLookupByLibrary.simpleMessage(""),
        "Recording": MessageLookupByLibrary.simpleMessage(""),
        "Reject": MessageLookupByLibrary.simpleMessage(""),
        "Rejoin": MessageLookupByLibrary.simpleMessage(""),
        "Remove": MessageLookupByLibrary.simpleMessage(""),
        "Remove all other devices": MessageLookupByLibrary.simpleMessage(""),
        "Remove device": MessageLookupByLibrary.simpleMessage(""),
        "Remove exile": MessageLookupByLibrary.simpleMessage(""),
        "Remove message": MessageLookupByLibrary.simpleMessage(""),
        "Render rich message content": MessageLookupByLibrary.simpleMessage(""),
        "Reply": MessageLookupByLibrary.simpleMessage(""),
        "Request permission": MessageLookupByLibrary.simpleMessage(""),
        "Request to read older messages":
            MessageLookupByLibrary.simpleMessage(""),
        "Revoke all permissions": MessageLookupByLibrary.simpleMessage(""),
        "Room has been upgraded": MessageLookupByLibrary.simpleMessage(""),
        "Saturday": MessageLookupByLibrary.simpleMessage(""),
        "Search for a chat": MessageLookupByLibrary.simpleMessage(""),
        "Seen a long time ago": MessageLookupByLibrary.simpleMessage(""),
        "Send": MessageLookupByLibrary.simpleMessage(""),
        "Send a message": MessageLookupByLibrary.simpleMessage(""),
        "Send file": MessageLookupByLibrary.simpleMessage(""),
        "Send image": MessageLookupByLibrary.simpleMessage(""),
        "Set a profile picture": MessageLookupByLibrary.simpleMessage(""),
        "Set group description": MessageLookupByLibrary.simpleMessage(""),
        "Set invitation link": MessageLookupByLibrary.simpleMessage(""),
        "Set status": MessageLookupByLibrary.simpleMessage(""),
        "Settings": MessageLookupByLibrary.simpleMessage(""),
        "Share": MessageLookupByLibrary.simpleMessage(""),
        "Sign up": MessageLookupByLibrary.simpleMessage(""),
        "Skip": MessageLookupByLibrary.simpleMessage(""),
        "Source code": MessageLookupByLibrary.simpleMessage(""),
        "Start your first chat :-)": MessageLookupByLibrary.simpleMessage(""),
        "Submit": MessageLookupByLibrary.simpleMessage(""),
        "Sunday": MessageLookupByLibrary.simpleMessage(""),
        "System": MessageLookupByLibrary.simpleMessage(""),
        "Tap to show menu": MessageLookupByLibrary.simpleMessage(""),
        "The encryption has been corrupted":
            MessageLookupByLibrary.simpleMessage(""),
        "They Don\'t Match": MessageLookupByLibrary.simpleMessage(""),
        "They Match": MessageLookupByLibrary.simpleMessage(""),
        "This room has been archived.":
            MessageLookupByLibrary.simpleMessage(""),
        "Thursday": MessageLookupByLibrary.simpleMessage(""),
        "Try to send again": MessageLookupByLibrary.simpleMessage(""),
        "Tuesday": MessageLookupByLibrary.simpleMessage(""),
        "Unblock Device": MessageLookupByLibrary.simpleMessage(""),
        "Unknown device": MessageLookupByLibrary.simpleMessage(""),
        "Unknown encryption algorithm":
            MessageLookupByLibrary.simpleMessage(""),
        "Unmute chat": MessageLookupByLibrary.simpleMessage(""),
        "Use Amoled compatible colors?":
            MessageLookupByLibrary.simpleMessage(""),
        "Username": MessageLookupByLibrary.simpleMessage(""),
        "Verify": MessageLookupByLibrary.simpleMessage(""),
        "Verify User": MessageLookupByLibrary.simpleMessage(""),
        "Video call": MessageLookupByLibrary.simpleMessage(""),
        "Visibility of the chat history":
            MessageLookupByLibrary.simpleMessage(""),
        "Visible for all participants":
            MessageLookupByLibrary.simpleMessage(""),
        "Visible for everyone": MessageLookupByLibrary.simpleMessage(""),
        "Voice message": MessageLookupByLibrary.simpleMessage(""),
        "Wallpaper": MessageLookupByLibrary.simpleMessage(""),
        "Wednesday": MessageLookupByLibrary.simpleMessage(""),
        "Welcome to the cutest instant messenger in the matrix network.":
            MessageLookupByLibrary.simpleMessage(""),
        "Who is allowed to join this group":
            MessageLookupByLibrary.simpleMessage(""),
        "Write a message...": MessageLookupByLibrary.simpleMessage(""),
        "Yes": MessageLookupByLibrary.simpleMessage(""),
        "You": MessageLookupByLibrary.simpleMessage(""),
        "You are invited to this chat":
            MessageLookupByLibrary.simpleMessage(""),
        "You are no longer participating in this chat":
            MessageLookupByLibrary.simpleMessage(""),
        "You cannot invite yourself": MessageLookupByLibrary.simpleMessage(""),
        "You have been banned from this chat":
            MessageLookupByLibrary.simpleMessage(""),
        "You won\'t be able to disable the encryption anymore. Are you sure?":
            MessageLookupByLibrary.simpleMessage(""),
        "Your own username": MessageLookupByLibrary.simpleMessage(""),
        "acceptedTheInvitation": m0,
        "activatedEndToEndEncryption": m1,
        "alias": MessageLookupByLibrary.simpleMessage("псевдонім"),
        "askSSSSCache": MessageLookupByLibrary.simpleMessage(
            "Будь ласка, введіть секретну фразу безпечного сховища або ключ відновлення для кешування ключів."),
        "askSSSSSign": MessageLookupByLibrary.simpleMessage(
            "Щоб мати можливість підписати іншу особу, будь ласка, введіть пароль або ключ відновлення вашого безпечного сховища."),
        "askSSSSVerify": MessageLookupByLibrary.simpleMessage(
            "Будь ласка, введіть вашу парольну фразу або ключ відновлення для підтвердження сеансу."),
        "askVerificationRequest": m2,
        "bannedUser": m3,
        "byDefaultYouWillBeConnectedTo": m4,
        "cachedKeys": MessageLookupByLibrary.simpleMessage(
            "Ключі було успішно збережено в кеші!"),
        "changedTheChatAvatar": m5,
        "changedTheChatDescriptionTo": m6,
        "changedTheChatNameTo": m7,
        "changedTheChatPermissions": m8,
        "changedTheDisplaynameTo": m9,
        "changedTheGuestAccessRules": m10,
        "changedTheGuestAccessRulesTo": m11,
        "changedTheHistoryVisibility": m12,
        "changedTheHistoryVisibilityTo": m13,
        "changedTheJoinRules": m14,
        "changedTheJoinRulesTo": m15,
        "changedTheProfileAvatar": m16,
        "changedTheRoomAliases": m17,
        "changedTheRoomInvitationLink": m18,
        "compareEmojiMatch": MessageLookupByLibrary.simpleMessage(""),
        "compareNumbersMatch": MessageLookupByLibrary.simpleMessage(""),
        "couldNotDecryptMessage": m19,
        "countParticipants": m20,
        "createdTheChat": m21,
        "crossSigningDisabled": MessageLookupByLibrary.simpleMessage(""),
        "crossSigningEnabled": MessageLookupByLibrary.simpleMessage(""),
        "dateAndTimeOfDay": m22,
        "dateWithYear": m23,
        "dateWithoutYear": m24,
        "emoteExists": MessageLookupByLibrary.simpleMessage(""),
        "emoteInvalid": MessageLookupByLibrary.simpleMessage(""),
        "emoteWarnNeedToPick": MessageLookupByLibrary.simpleMessage(""),
        "groupWith": m25,
        "hasWithdrawnTheInvitationFor": m26,
        "incorrectPassphraseOrKey": MessageLookupByLibrary.simpleMessage(""),
        "inviteContactToGroup": m27,
        "inviteText": m28,
        "invitedUser": m29,
        "is typing...": MessageLookupByLibrary.simpleMessage(""),
        "isDeviceKeyCorrect": MessageLookupByLibrary.simpleMessage(""),
        "joinedTheChat": m30,
        "keysCached": MessageLookupByLibrary.simpleMessage(""),
        "keysMissing": MessageLookupByLibrary.simpleMessage(""),
        "kicked": m31,
        "kickedAndBanned": m32,
        "lastActiveAgo": m33,
        "loadCountMoreParticipants": m34,
        "logInTo": m35,
        "newVerificationRequest": MessageLookupByLibrary.simpleMessage(""),
        "noCrossSignBootstrap": MessageLookupByLibrary.simpleMessage(""),
        "noMegolmBootstrap": MessageLookupByLibrary.simpleMessage(""),
        "numberSelected": m36,
        "ok": MessageLookupByLibrary.simpleMessage(""),
        "onlineKeyBackupDisabled": MessageLookupByLibrary.simpleMessage(""),
        "onlineKeyBackupEnabled": MessageLookupByLibrary.simpleMessage(""),
        "passphraseOrKey": MessageLookupByLibrary.simpleMessage(""),
        "play": m37,
        "redactedAnEvent": m38,
        "rejectedTheInvitation": m39,
        "removedBy": m40,
        "seenByUser": m41,
        "seenByUserAndCountOthers": m42,
        "seenByUserAndUser": m43,
        "sentAFile": m44,
        "sentAPicture": m45,
        "sentASticker": m46,
        "sentAVideo": m47,
        "sentAnAudio": m48,
        "sessionVerified": MessageLookupByLibrary.simpleMessage(""),
        "sharedTheLocation": m49,
        "timeOfDay": m50,
        "title": MessageLookupByLibrary.simpleMessage(""),
        "unbannedUser": m51,
        "unknownEvent": m52,
        "unknownSessionVerify": MessageLookupByLibrary.simpleMessage(""),
        "unreadChats": m53,
        "unreadMessages": m54,
        "unreadMessagesInChats": m55,
        "userAndOthersAreTyping": m56,
        "userAndUserAreTyping": m57,
        "userIsTyping": m58,
        "userLeftTheChat": m59,
        "userSentUnknownEvent": m60,
        "verifiedSession": MessageLookupByLibrary.simpleMessage(""),
        "verifyManual": MessageLookupByLibrary.simpleMessage(""),
        "verifyStart": MessageLookupByLibrary.simpleMessage(""),
        "verifySuccess": MessageLookupByLibrary.simpleMessage(""),
        "verifyTitle": MessageLookupByLibrary.simpleMessage(""),
        "waitingPartnerAcceptRequest": MessageLookupByLibrary.simpleMessage(""),
        "waitingPartnerEmoji": MessageLookupByLibrary.simpleMessage(""),
        "waitingPartnerNumbers": MessageLookupByLibrary.simpleMessage("")
      };
}
